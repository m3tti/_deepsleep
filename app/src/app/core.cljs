(ns app.core
    (:require
      [reagent.core :as r]))


;; -------------------------
;; Business

(def context
  (let [constructor (or js/window.AudioContext
                        js/window.webkitAudioContext)]
    (constructor.)))
(def pink-noise "pinkNoise")
(def currentTimeoutId (atom nil))
(def started (r/atom false))
(def interval (r/atom 0.5))
(def intensity (r/atom 0.5))
(def sleep-time (r/atom 1))
(def page (r/atom "HOME"))

(def cordova js/cordova)

(defn device-ready [fun]
    (.addEventListener js/document "deviceready" fun))

(defn startup []
  (.attach js/FastClick (.-body js/document))
  (.enable js/cordova.plugins.backgroundMode)
  (.on js/cordova.plugins.backgroundMode "enable" #(println %))
  (.preloadComplex js/window.plugins.NativeAudio
                   pink-noise
                   "audio/pinkNoise.mp3"
                   1 1 0)
  )

(defn timeout [fun delay]
  (reset! currentTimeoutId (js/setTimeout fun delay)))

(defn hour-to-milliseconds [hour]
  (* hour 60 60 1000))

(defn minutes-to-milliseconds [minutes]
  (* minutes 60 1000))

(defn toggle-noise [noise on]
  "toggels a given noise"
  (if on
    (.loop js/window.plugins.NativeAudio noise)
    (.stop js/window.plugins.NativeAudio noise)
    ))

(defn burst []
  "starts a burst interval"
  (when @started
    (do
      (toggle-noise pink-noise true)
      (timeout (fn []
                 (toggle-noise pink-noise false)
                 (timeout #(burst) (hour-to-milliseconds @interval)))
               (minutes-to-milliseconds @intensity)))))


(defn toggle-burst []
  (swap! started not)
  (if @started
    (timeout #(burst) (hour-to-milliseconds @sleep-time))
    (do
      (js/clearTimeout @currentTimeoutId)
      (toggle-noise pink-noise false))))


;; -------------------------
;; Views

(defn render-infos []
  [:div
   [:table.u-full-width
    [:tr [:td [:strong "Start:"]] [:td (str @sleep-time " h")]]
    [:tr [:td [:strong "Interval:"]] [:td (str @interval " h")]]
    [:tr [:td [:strong "Intesity:"]] [:td (str @intensity " m")]]
    ]])

(defn atom-input [atom text id]
  [:div
    [:label { :for id } text]
    [:input.u-full-width {:type "text"
             :on-change #(reset! atom (-> % .-target .-value))
             :value @atom
             :id id}]])

(defn settings-page []
  (let [convert-atom #(reset! % (js/parseFloat @%))
        back #(reset! page "HOME")]
    [:div
     (atom-input sleep-time "Start (in h)" 2)
     (atom-input interval "Interval (in h)" 1)
     (atom-input intensity "Intensity (in m)" 3)
     [:input.u-full-width {:type "button"
                           :on-click #(do
                                        (toggle-noise pink-noise true)
                                        (timeout (fn [] (toggle-noise pink-noise false)) (minutes-to-milliseconds @intensity)))
                           :value "Test Intensity"}]
     [:Input.u-full-width {:type "button"
                           :on-click #(do
                                        (map convert-atom [interval intensity sleep-time])
                                        (back))
                           :value "Back"}]]))


(defn home-page []
  [:div
   (render-infos)
   [:input.u-full-width {:type "button"
                         :on-click #(toggle-burst)
                         :value (if @started
                                  "stop"
                                  "start")}]
   [:input.u-full-width {:type "button"
                         :on-click #(reset! page "SETTINGS")
                         :value "Settings"}]])

(defn main []
  [:div.container.animated [:h1 { :style { :text-align "center" } } "_DeepSleep"]
   (cond
     (= @page "SETTINGS") (settings-page)
     (= @page "HOME") (home-page)
     :else (home-page))])

;; -------------------------
;; Initialize app

(defn mount-root []
  (r/render [main] (.getElementById js/document "app")))

(defn init! []
  (mount-root)
  (device-ready (fn []
                  (startup))))
