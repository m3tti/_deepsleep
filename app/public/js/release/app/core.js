// Compiled by ClojureScript 1.9.671 {:static-fns true, :optimize-constants true}
goog.provide('app.core');
goog.require('cljs.core');
goog.require('cljs.core.constants');
goog.require('reagent.core');
app.core.context = (function (){var constructor$ = (function (){var or__6986__auto__ = window.AudioContext;
if(cljs.core.truth_(or__6986__auto__)){
return or__6986__auto__;
} else {
return window.webkitAudioContext;
}
})();
return (new constructor$());
})();
app.core.pink_noise = app.core.context.createPinkNoise();
app.core.started = reagent.core.atom.cljs$core$IFn$_invoke$arity$1(false);
app.core.interval = reagent.core.atom.cljs$core$IFn$_invoke$arity$1((((60) * (30)) * (1000)));
app.core.intensity = reagent.core.atom.cljs$core$IFn$_invoke$arity$1((5000));
app.core.sleep_time = reagent.core.atom.cljs$core$IFn$_invoke$arity$1((((60) * (60)) * (1000)));
app.core.toggle_noise = (function app$core$toggle_noise(noise,on){

if(cljs.core.truth_(on)){
return noise.connect(app.core.context.destination);
} else {
return noise.disconnect();
}
});
app.core.burst = (function app$core$burst(){

if(cljs.core.truth_(cljs.core.deref(app.core.started))){
app.core.toggle_noise(app.core.pink_noise,true);

var G__9926 = (function (){
app.core.toggle_noise(app.core.pink_noise,false);

var G__9928 = (function (){
return (app.core.burst.cljs$core$IFn$_invoke$arity$0 ? app.core.burst.cljs$core$IFn$_invoke$arity$0() : app.core.burst.call(null));
});
var G__9929 = cljs.core.deref(app.core.interval);
return setTimeout(G__9928,G__9929);
});
var G__9927 = cljs.core.deref(app.core.intensity);
return setTimeout(G__9926,G__9927);
} else {
return null;
}
});
app.core.toggle_burst = (function app$core$toggle_burst(){
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$2(app.core.started,cljs.core.not);

if(cljs.core.truth_(cljs.core.deref(app.core.started))){
var G__9930 = (function (){
return app.core.burst();
});
var G__9931 = cljs.core.deref(app.core.sleep_time);
return setTimeout(G__9930,G__9931);
} else {
return null;
}
});
app.core.home_page = (function app$core$home_page(){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.cst$kw$div$container,new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.cst$kw$h1,new cljs.core.PersistentArrayMap(null, 1, [cljs.core.cst$kw$style,new cljs.core.PersistentArrayMap(null, 1, [cljs.core.cst$kw$text_DASH_align,"center"], null)], null),"_DeepSleep"], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.cst$kw$input$u_DASH_full_DASH_width,new cljs.core.PersistentArrayMap(null, 3, [cljs.core.cst$kw$type,"button",cljs.core.cst$kw$on_DASH_click,(function (){
return app.core.toggle_burst();
}),cljs.core.cst$kw$value,(cljs.core.truth_(cljs.core.deref(app.core.started))?"stop":"start")], null)], null)], null);
});
app.core.mount_root = (function app$core$mount_root(){
return reagent.core.render.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [app.core.home_page], null),document.getElementById("app"));
});
app.core.init_BANG_ = (function app$core$init_BANG_(){
return app.core.mount_root();
});
