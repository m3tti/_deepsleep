// Compiled by ClojureScript 1.9.671 {:static-fns true, :optimize-constants true}
goog.provide('reagent.debug');
goog.require('cljs.core');
goog.require('cljs.core.constants');
reagent.debug.has_console = typeof console !== 'undefined';
reagent.debug.tracking = false;
if(typeof reagent.debug.warnings !== 'undefined'){
} else {
reagent.debug.warnings = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(null);
}
if(typeof reagent.debug.track_console !== 'undefined'){
} else {
reagent.debug.track_console = (function (){var o = ({});
o.warn = ((function (o){
return (function() { 
var G__12139__delegate = function (args){
return cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$variadic(reagent.debug.warnings,cljs.core.update_in,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.cst$kw$warn], null),cljs.core.conj,cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.str,args)], 0));
};
var G__12139 = function (var_args){
var args = null;
if (arguments.length > 0) {
var G__12140__i = 0, G__12140__a = new Array(arguments.length -  0);
while (G__12140__i < G__12140__a.length) {G__12140__a[G__12140__i] = arguments[G__12140__i + 0]; ++G__12140__i;}
  args = new cljs.core.IndexedSeq(G__12140__a,0,null);
} 
return G__12139__delegate.call(this,args);};
G__12139.cljs$lang$maxFixedArity = 0;
G__12139.cljs$lang$applyTo = (function (arglist__12141){
var args = cljs.core.seq(arglist__12141);
return G__12139__delegate(args);
});
G__12139.cljs$core$IFn$_invoke$arity$variadic = G__12139__delegate;
return G__12139;
})()
;})(o))
;

o.error = ((function (o){
return (function() { 
var G__12142__delegate = function (args){
return cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$variadic(reagent.debug.warnings,cljs.core.update_in,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.cst$kw$error], null),cljs.core.conj,cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.str,args)], 0));
};
var G__12142 = function (var_args){
var args = null;
if (arguments.length > 0) {
var G__12143__i = 0, G__12143__a = new Array(arguments.length -  0);
while (G__12143__i < G__12143__a.length) {G__12143__a[G__12143__i] = arguments[G__12143__i + 0]; ++G__12143__i;}
  args = new cljs.core.IndexedSeq(G__12143__a,0,null);
} 
return G__12142__delegate.call(this,args);};
G__12142.cljs$lang$maxFixedArity = 0;
G__12142.cljs$lang$applyTo = (function (arglist__12144){
var args = cljs.core.seq(arglist__12144);
return G__12142__delegate(args);
});
G__12142.cljs$core$IFn$_invoke$arity$variadic = G__12142__delegate;
return G__12142;
})()
;})(o))
;

return o;
})();
}
reagent.debug.track_warnings = (function reagent$debug$track_warnings(f){
reagent.debug.tracking = true;

cljs.core.reset_BANG_(reagent.debug.warnings,null);

(f.cljs$core$IFn$_invoke$arity$0 ? f.cljs$core$IFn$_invoke$arity$0() : f.call(null));

var warns = cljs.core.deref(reagent.debug.warnings);
cljs.core.reset_BANG_(reagent.debug.warnings,null);

reagent.debug.tracking = false;

return warns;
});
